SafeVoid
========

Bukkit (Minecraft) plugin. Continuation of NoVoid. Instead of using onPlayerMove listener, it uses onEntityDamaged to save some performance. To also save more performance, it makes a bedrock to stop the player from continuously falling. I also know that WorldGuard has this function already. This'll be for servers with no WG or if the WG void protection is not working. However, if I am not wrong, WG doesn't patch up the hole to the void for you, this does though.

Copyrights
------------

NoVoid - A Bukkit void damage disabler.

Copyright � 2012 Jaryl Chng


This source code is free and for the public: you can only redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.


This source code is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License <http://www.gnu.org/licenses/> for more details.


If you wish to modify, do not in any way distribute the modified plugin unless you have my consent. However, you may use it for private use or you could look at the "Want to contribute?" section of this readme file.

Want to contribute?
--------------------

If you are a Java coder, You can fork this source code on GitHub, edit and then post a pull request. I will take a look occationally and see if it is good to implement or not. If you are not though, you can just go on the BukkitDev or the forum post to post your suggestion.


By forking and pulling, you must agree to license your code under the "GNU General Public License v3".