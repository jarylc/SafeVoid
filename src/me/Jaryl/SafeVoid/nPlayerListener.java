package me.Jaryl.SafeVoid;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class nPlayerListener implements Listener {
	public static SafeVoid plugin;
	
	public nPlayerListener(SafeVoid instance) {
		plugin = instance;
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onEntityDamage(EntityDamageEvent event) {
		if (plugin.plEnabled && !event.isCancelled() && event.getEntity() instanceof Player && event.getCause() == DamageCause.VOID)
		{
			Player player = (Player)event.getEntity();
			if (!plugin.blacklistWorlds.contains(player.getWorld().getName()) || (player.getGameMode() == GameMode.CREATIVE && plugin.creativeAll))
			{
				if (player.isInsideVehicle()) {
					player.getVehicle().eject();
					player.getVehicle().remove();
				}
				
				if (plugin.toSpawn)
				{
					player.teleport(player.getWorld().getSpawnLocation());
				}
				else
				{
					int highestY = player.getWorld().getHighestBlockYAt(player.getLocation());
					Location loc = new Location(player.getWorld(), player.getLocation().getX(), highestY+5, player.getLocation().getZ());
				    Block block = player.getWorld().getBlockAt(loc.getBlockX(), 0, loc.getBlockZ());
				    block.setType(Material.BEDROCK);
					player.teleport(loc);
				}
				
				player.setFallDistance(0);
				event.setCancelled(true);
			}
		}
	}
}
