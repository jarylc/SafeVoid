package me.Jaryl.SafeVoid;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class SafeVoid extends JavaPlugin {
	public Boolean plEnabled = true;
	
	private final nPlayerListener playerListener = new nPlayerListener(this);
	public List<String> blacklistWorlds = new ArrayList<String>();
	
	public PermissionsHandler PermHandler = new PermissionsHandler(this);
	public Boolean Perms = true;
	
	public Configuration config;
	public Boolean creativeAll;
	public Boolean toSpawn;
	
	private void loadConfigurations(CommandSender p)
	{
    	Configuration config = new Configuration(this);
    	
    	if (config.exists())
    	{
    		config.load();
    	}
    	creativeAll = config.parse("creative_all_worlds", true);
    	toSpawn = config.parse("teleport_to_spawn", false);
    	
    	if (blacklistWorlds.isEmpty())
    	{
    		blacklistWorlds.add("lorem");
    		blacklistWorlds.add("lpsum");
    	}
    	blacklistWorlds = config.parse("blacklisted_worlds", blacklistWorlds);
    	
    	config.save();
    	
    	if (p != null)
    	{
    		p.sendMessage(ChatColor.AQUA + "[SafeVoid] New configurations:");
    		printConfig(p);
    	}
	}
	private void printConfig(CommandSender p)
	{
		p.sendMessage("    Affect creative mode for all worlds: " + creativeAll);
		p.sendMessage("    Teleport to spawn: " + Perms);
		p.sendMessage("    Ignored worlds: " + blacklistWorlds.toString());
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args){
		if(commandLabel.equalsIgnoreCase("safevoid") || commandLabel.equalsIgnoreCase("sv")) {
			if(((sender instanceof Player) && PermHandler.hasPermission((Player)sender, "savevoid.admin", false)) || !(sender instanceof Player)) {
				if(args.length == 0) {
					sender.sendMessage(ChatColor.GOLD + "[SafeVoid] Enabled: " + plEnabled);
					sender.sendMessage(ChatColor.AQUA + "[SafeVoid] Commands:");
					sender.sendMessage("    /" + commandLabel);
					sender.sendMessage("        toggle - Toggle plugin");
					sender.sendMessage("        reload - Reloads plugin");
					sender.sendMessage("        config - Print configurations");
				}
				else
				{
					if (args[0].equalsIgnoreCase("toggle"))
					{
						plEnabled = !plEnabled;
						sender.sendMessage(ChatColor.GOLD + "[SafeVoid] Enabled: " + plEnabled);
						return true;
					}
					if (args[0].equalsIgnoreCase("reload"))
					{
						loadConfigurations(sender);
						return true;
					}
					if (args[0].equalsIgnoreCase("config"))
					{
						sender.sendMessage(ChatColor.AQUA + "[SafeVoid] Configurations:");
						printConfig(sender);
						return true;
					}
					
					sender.sendMessage(ChatColor.RED + "[SafeVoid] Unknown command: " + args[0]);
				}
			}
			else
			{
				sender.sendMessage(ChatColor.RED + "[SafeVoid] You do not have permissions to do this!");
			}
			return true;
		}
		return false;
	}
    
    public void onDisable() {
    	System.out.println("[SafeVoid v" + this.getDescription().getVersion() + "] Disabled.");
    }
    
    public void onEnable() {
    	loadConfigurations(null);
    	PermHandler.setupPermissions();
    	
    	
    	PluginManager pm = getServer().getPluginManager();
    	pm.registerEvents(playerListener, this);
    	//pm.registerEvent(Event.Type.ENTITY_DAMAGE, playerListener, Event.Priority.Normal, this);
    	
    	System.out.println("[SafeVoid v" + this.getDescription().getVersion() + "] Enabled.");
    }
}
