package me.Jaryl.SafeVoid;

import org.bukkit.entity.Player;
import ru.tehkode.permissions.PermissionManager;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class PermissionsHandler {
	private SafeVoid plugin; //NOTE TO SELF: RMB TO CHANGE THIS ON NEW PROJECTS
	public PermissionsHandler(SafeVoid pl) { //NOTE TO SELF: RMB TO CHANGE THIS ON NEW PROJECTS
		plugin = pl;
	}

    // PermissionsEX
    public PermissionManager pexPermissions;
    Boolean PEXB = false;
	
	public boolean hasPermission(Player p, String perm, boolean def)
	{
		if (plugin.Perms)
		{
			if (PEXB) // PEX
				return pexPermissions.has(p, perm);
			
			return p.hasPermission(perm);
		}
		
		return (p.isOp() || def);
	}
	
	public void setupPermissions() {
        if (plugin.getServer().getPluginManager().getPlugin("PermissionsEx") != null) {
            pexPermissions = PermissionsEx.getPermissionManager();
            PEXB = true;
            System.out.println("[" + plugin.getDescription().getName() + "] Listening to PermissionsEX.");
            return;
        }
        
        if (plugin.getServer().getPluginManager().getPlugin("bPermissions") != null) {
            System.out.println("[" + plugin.getDescription().getName() + "] Listening to bPermissions.");
            return;
        }
        
        System.out.println("[" + plugin.getDescription().getName() + "] No custom permission plugins found, using original permissions.");
    }
}
